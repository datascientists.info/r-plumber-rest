FROM r-base:latest

LABEL info.datescientists.data.plumber="1.0.0"
LABEL info.datescientists.data.plumber.release_date="2019-02-09"
MAINTAINER Marc Matt

RUN mkdir -p /r/plumber
WORKDIR /r/plumber

COPY . /r/plumber

RUN Rscript requirements.R

CMD ["Rscript", "startServer.R"]
