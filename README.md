---
title: "REST API for Sentiment Analysis"
author: "Marc Matt"
date: "13 Juni 2018"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Usage

### Prerequisits
1. install R
2. run _requirements.R_ file
3. run _startServer.R_ file

### Use REST-API locally
1. open your browser at http://127.0.0.1:8001/sentiment?msg="Text to analyse"
2. Replace "Text to analyse" with the text you want to analyse
3. The answer will be a json of the following format:
    {"sentiment":[{"sentiment":"anger"}]}
    or in case of no sentiment found
    {"sentiment":["no sentiment found"]}


### Deploy using a Docker container
* Added Dockerfile for deploying the API

